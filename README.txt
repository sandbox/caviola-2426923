CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Known issues


INTRODUCTION
------------
The Content Tree module enables you to organize your site's content in a big, 
fast, unlimited-depth hierarchy of arbitrary content types.
You can think of Content tree as Drupal menus on steroids.

This module supports the following features:

    * Administrative interface
        - Browse the content tree.
        - Add nodes of any content type in any location in the tree.
        - Add symbolic links to any Drupal menu path or external links. 
          This allows you to, for instance, have the same Drupal node appear 
          in different content tree locations..
        - Bulk delete tree nodes. A subtree must be empty before it can be 
          deleted.
        - Hide/show tree nodes/subnodes.
        - Reorder nodes using drag & drop.
    

    * Automatic path aliases and default locations
        - For each content type you can define a Token-based pattern 
          that will be used to construct a hierarchical path alias 
          to the corresponding content tree node. 
          For example: you can define that Page nodes use the pattern 
          [title-raw] whereas for Blog nodes you can use 
          [yyyy]/[mm]/[dd]/[title-raw].
        - You can also define several default content tree locations 
          for each node type. Then when creating a node in the tree, 
          another copy will be automatically created at all the defined 
          locations.


    * Content tree blocks
        - Create Menu block-style blocks of any content subtree.


    * Views integration
        -You can use the content tree as a primary table for your views. 
         All node-related fields/filters are readily available.


REQUIREMENTS
------------
This module requires the following modules:
 * Token (https://drupal.org/project/token)
 * Path (https://drupal.org/project/path)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/documentation/install/modules-themes/modules-5-6
   for further information.


KNOWN ISSUES
------------
The module is still in development stage and has been released to get feedback 
from the community. It HAS NOT been extensively tested.
