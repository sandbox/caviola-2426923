<?php

/**
 * @file
 * Implement utility functions needed by the administration pages.
 */

define('CONTENT_TREE_OVERVIEW_PAGE_LEN', 40);

/**
 * Return the menu callbacks.
 */
function _content_tree_menu() {
  $items['ct_node/%content_tree_node'] = array(
    'access callback' => 'node_access',
    'access arguments' => array('view', 1),
    'page arguments' => array(1),
    'page callback' => 'content_tree_node_page_view',
    'type' => MENU_CALLBACK,
  );
  $items['ct_node/add/%'] = array(
    'access arguments' => array('create', 2),
    'access callback' => 'node_access',
    'page arguments' => array(2, 3),
    'page callback' => 'content_tree_node_page_add',
    'type' => MENU_CALLBACK,
    'file' => 'includes/content_tree.admin.inc',
  );
  $items['ct_node/%content_tree_node/edit'] = array(
    'access arguments' => array('update', 1),
    'access callback' => 'node_access',
    'page arguments' => array(1),
    'page callback' => 'content_tree_node_page_edit',
    'type' => MENU_CALLBACK,
    'file' => 'includes/content_tree.admin.inc',
  );
  $items['ct_item/%content_tree_item/edit'] = array(
    'access arguments' => array('administer content tree links'),
    'page arguments' => array(1),
    'page callback' => 'content_tree_item_edit_page',
    'type' => MENU_CALLBACK,
    'file' => 'includes/content_tree.admin.inc',
  );
  $items['ct_item/%content_tree_item'] = array(
    'access arguments' => array('access content'),
    'page arguments' => array(1),
    'page callback' => 'content_tree_item_page_view',
    'type' => MENU_CALLBACK,
  );
  $items['ct_item/add'] = array(
    'access arguments' => array('administer content tree links'),
    'page arguments' => array(2),
    'page callback' => 'content_tree_item_add_page',
    'type' => MENU_CALLBACK,
    'file' => 'includes/content_tree.admin.inc',
  );
  $items['admin/content_tree'] = array(
    'access arguments' => array('administer content tree'),
    'title' => t('Content tree'),
    'page arguments' => array('content_tree_admin_form'),
    'page callback' => 'drupal_get_form',
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/content_tree.admin.inc',
  );
  $items['admin/content_tree/newblock'] = array(
    'title' => t('Add block'),
    'page arguments' => array('content_tree_add_block_form'),
    'page callback' => 'drupal_get_form',
    'access arguments' => array('administer blocks'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/content_tree.block.inc',
  );
  return $items;
}

/**
 * Return either the content tree admin form or item delete confirmation form.
 */
function content_tree_admin_form($form_state) {
  // If we are being called because the "delete" button was pressed,
  // return the confirmation form.
  if (!empty($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
    return content_tree_admin_delete_confirm($form_state);
  }

  return _content_tree_admin_form($form_state);
}

/**
 * Build the content tree admin form.
 */
function _content_tree_admin_form($form_state) {
  $parent_iid = (int) arg(2);

  // This query returns all "unflagged" 1st-level children of $parent_iid.
  $query = '
    SELECT
      i.iid,
      i.nid,
      IFNULL(i.item_title, n.title) AS title,
      i.item_path,
      n.type,
      i.weight,
      i.is_visible
    FROM {content_tree_item} i
      LEFT JOIN {node} n USING(nid)
    WHERE i.parent_iid = %d
    ORDER BY i.weight';

  $count_query = 'SELECT COUNT(*) FROM {content_tree_item} WHERE parent_iid = %d';
  $result = pager_query($query, CONTENT_TREE_OVERVIEW_PAGE_LEN, 0, $count_query, $parent_iid);

  $form['#tree'] = TRUE;

  if ($parent_iid && ($parent_link = content_tree_get_path_alias($parent_iid))) {
    $form['parent_link'] = array('#value' => l($parent_link, $parent_link));
  }

  $items = array();
  $content_types = node_get_types('names');
  $link_attributes = array('query' => drupal_get_destination());

  while ($item = db_fetch_object($result)) {
    $items[$item->iid] = '';

    $edit_link = empty($item->nid) ?
        l(t('edit'), "ct_item/$item->iid/edit", $link_attributes) :
        l(t('edit'), "ct_node/$item->iid/edit", $link_attributes);

    $form['weight'][$item->iid] = array(
      '#type' => 'weight',
      '#default_value' => $item->weight,
      '#attributes' => array('class' => 'weight'),
    );
    $form['title'][$item->iid] = array(
      '#value' => l($item->title, "admin/content_tree/$item->iid"),
    );
    $form['type'][$item->iid] = array(
      '#value' => !empty($content_types[$item->type]) ? $content_types[$item->type] : t('<i>content tree link</i>'),
    );
    $form['is_visible'][$item->iid] = array(
      '#value' => $item->is_visible ? t('visible') : t('not visible'),
    );
    $form['edit'][$item->iid] = array('#value' => $edit_link);
  }
  $form['breadcrumb'] = array('#value' => _content_tree_admin_breadcrumb($parent_iid));
  $form['menu'] = array('#value' => _content_tree_admin_menu($parent_iid));

  if ($items) {
    $form['items'] = array(
      '#type' => 'checkboxes',
      '#options' => $items,
    );
    $form['update'] = array(
      '#type' => 'fieldset',
      '#title' => t('Update options'),
      '#tree' => FALSE,
    );
    $form['update']['operation'] = array(
      '#type' => 'select',
      '#options' => array(
        'reorder' => t('Reorder'),
        'show' => t('Show'),
        'hide' => t('Hide'),
        'delete' => t('Delete'),
      ),
      '#default_value' => 'reorder',
    );
    $form['update']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
      '#submit' => array('content_tree_admin_update_submit'),
      '#validate' => array('content_tree_admin_update_validate'),
    );
    // TODO: copy/move subtree fields.
  }
  $form['#theme'] = 'content_tree_admin_form';

  return $form;
}

/**
 * Build the item delete confirmation form.
 */
function content_tree_admin_delete_confirm(&$form_state) {
  $form['items'] = array('#type' => 'value', '#value' => array_filter($form_state['values']['items']));
  $form['#submit'] = array('content_tree_admin_delete_confirm_submit');

  return confirm_form(
      $form, t('Are you sure you want to delete the item(s)?'), 'admin/content_tree', t('<p>This operation will <strong>delete the selected item(s) and all associated nodes</strong>. This action cannot be undone.</p>'), t('Delete all'), t('Cancel')
  );
}

/**
 * Submit handler for the item delete confirmation form.
 *
 * This either start a batch process or delete the items immediately
 * depending on how many we're deleting.
 */
function content_tree_admin_delete_confirm_submit($form, &$form_state) {
  if (count($form_state['values']['items']) > CONTENT_TREE_BATCH_DELETE_THRESHOLD) {
    content_tree_delete_batch_process($form_state['values']['items']);
  }
  else {
    $deleted = 0;
    foreach ($form_state['values']['items'] as $iid) {
      if (content_tree_item_delete($iid)) {
        ++$deleted;
      }
    }
    if ($deleted) {
      drupal_set_message(t('!count content tree item(s) deleted successfully.', array('!count' => $deleted)));
    }
  }
}

/**
 * Validator function for the content tree admin form.
 */
function content_tree_admin_update_validate($element, &$form_state) {
  $items = array_filter($form_state['values']['items']);
  // All "Update" operations except "reorder" need at least one item.
  if (!$items && $form_state['values']['operation'] != 'reorder') {
    form_set_error('', t('You must select at least one item to perform the requested operation.'));
  }
}

/**
 * Submit handler for content tree admin form.
 */
function content_tree_admin_update_submit($form, &$form_state) {
  switch (($operation = $form_state['values']['operation'])) {
    case 'reorder':
      // Update the weight of each item only if it has changed.
      foreach ($form_state['values']['weight'] as $iid => $weight) {
        if ($weight != $form['weight'][$iid]['#default_value']) {
          db_query('UPDATE {content_tree_item} SET weight = %d WHERE iid = %d', $weight, $iid);
        }
      }
      drupal_set_message(t('The items have been reordered successfully.'));
      break;

    case 'show':
    case 'hide':
      $operation_visibility = array(
        'show' => 1,
        'hide' => 0,
      );
      foreach ($form_state['values']['items'] as $iid => $checked) {
        if ($checked) {
          content_tree_set_visibility($iid, $operation_visibility[$operation]);
        }
      }
      drupal_set_message(t('The visibility of the items has been successfully updated.'));
      break;

    case 'delete':
      // Tell the FAPI to rebuild the form.
      // This will result in "content_tree_admin_form" being called again,
      // but this time it will return a confirmation form.
      $form_state['rebuild'] = TRUE;
      break;
  }
}

/**
 * Submit handler for move subtree form.
 */
function content_tree_admin_move_submit($form, &$form_state) {
  // TODO:
}

/**
 * Submit handler for copy subtree form.
 */
function content_tree_admin_copy_submit($form, &$form_state) {
  // TODO:
}

/**
 * Render a breadcrumb with links to all the ancestors of the given item.
 */
function _content_tree_admin_breadcrumb($iid) {
  $trail = content_tree_get_trail($iid);
  if (empty($trail['items'])) {
    return '';
  }

  // Pop off the last item because we won't show it as a link.
  $last = array_pop($trail['items']);

  // Add a link to view the tree's root.
  $pieces = array(l(t('<root>'), 'admin/content_tree'));
  foreach ($trail['items'] as $piece) {
    $pieces[] = l($piece['title'], "admin/content_tree/{$piece['iid']}");
  }

  $pieces[] = $last['title'];

  return '<div id="ContentTree_Breadcrumb">' . implode(' / ', $pieces) . '</div>';
}

/**
 * Render the "Create content here" menu.
 */
function _content_tree_admin_menu($parent_iid) {
  $link_attributes = array('query' => drupal_get_destination());
  $output = '<ul id="ContentTree_Menu"><li><a class="title" href="#">' . t('Create content here:') . '</a><ul><li><i>' . l(t('Content tree link'), 'ct_item/add' . ($parent_iid ? "/$parent_iid" : ''), $link_attributes) . '</i></li>';

  foreach (node_get_types('names') as $type_id => $type_name) {
    if (user_access('administer nodes') || user_access("create $type_id content")) {
      $output .= '<li>' . l($type_name, "ct_node/add/$type_id" . ($parent_iid ? "/$parent_iid" : ''), $link_attributes) . '</li>';
    }
  }

  $output .= '</ul></li></ul>';
  return $output;
}

/**
 * Menu callback for ct_node/add/%.
 */
function content_tree_node_page_add($type, $parent_iid = 0) {
  global $user;

  $types = node_get_types('names');
  $type = str_replace('-', '_', $type);
  if (!isset($types[$type])) {
    return;
  }

  $node = array(
    'type' => $type,
    'uid' => $user->uid,
    'name' => isset($user->name) ? $user->name : '',
    'language' => '',
    'content_tree' => array('parent_iid' => (int) $parent_iid),
  );

  module_load_include('inc', 'node', 'node.pages');
  drupal_set_title(t('Create content tree node <i>@type</i>', array('@type' => $types[$type])));
  return drupal_get_form($type . '_node_form', $node);
}

/**
 * Menu callback for ct_node/%content_tree_node/edit.
 */
function content_tree_node_page_edit($node) {
  module_load_include('inc', 'node', 'node.pages');
  drupal_set_title(check_plain($node->title));
  return drupal_get_form($node->type . '_node_form', $node);
}

/**
 * Build the content tree add item form.
 */
function content_tree_item_form(&$form_state, $item) {
  $form['iid'] = array(
    '#type' => 'value',
    '#value' => $item['iid'],
  );
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $item['uid'],
  );
  $form['parent_iid'] = array(
    '#type' => 'value',
    '#value' => $item['parent_iid'],
  );
  $form['item_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $item['item_title'],
    '#weight' => 0,
  );
  $form['item_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#description' => t('This must be a system path for internal links. For external links, prefix the path with <b>http://</b>.'),
    '#required' => TRUE,
    '#default_value' => $item['item_path'],
    '#weight' => 1,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#description' => t('This value is used to construct the path alias for this item and also serves as an identification string. If left empty, it will be generated from <strong>Title</strong>.'),
    '#title' => t('Name'),
    '#default_value' => $item['name'],
    '#weight' => 2,
  );
  $form['is_visible'] = array(
    '#type' => 'checkbox',
    '#title' => t('Visible'),
    '#description' => t('Whether the item is shown in item listings.'),
    '#default_value' => $item['is_visible'],
    '#weight' => 3,
  );
  $form['children'] = array(
    '#type' => 'checkbox',
    '#title' => t('Leaf'),
    '#description' => t('Select if this item cannot have subitems. If <strong>Path</strong> is an external link, this will be set automatically.'),
    '#default_value' => $item['children'],
    '#return_value' => -1,
    '#weight' => 4,
  );
  $form['is_external'] = array('#type' => 'value', '#value' => 0);
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
  );
  return $form;
}

/**
 * Validator function for the content tree add item form.
 */
function content_tree_item_form_validate($form, &$form_state) {
  $item = &$form_state['values'];
  if (empty($item['item_path'])) {
    return;
  }

  if (menu_path_is_external($item['item_path'])) {
    $item['is_external'] = 1;
    return;
  }

  $normal_path = drupal_get_normal_path($item['item_path']);
  if (preg_match('/^(ct_item|ct_node)\//', $item['item_path'])) {
    form_set_error('item_path', t('You cannot use the path of a content tree item.'));
    return;
  }

  if ($normal_path != $item['item_path']) {
    drupal_set_message(t('%item_path has been stored as %normal_path', array('%item_path' => $item['item_path'], '%normal_path' => $normal_path)));
    $item['item_path'] = $normal_path;
  }
}

/**
 * Submit handler for the content tree add item form.
 */
function content_tree_item_form_submit($form, &$form_state) {
  $item = $form_state['values'];
  // Generate the item's name if it was left empty.
  if ($item['name'] == '') {
    $item['name'] = _content_tree_sanitize_item_name($item['item_title']);
  }

  if (empty($item['iid'])) {
    content_tree_insert($item);
    // TODO: drupal_set_message
  }
  else {
    content_tree_update($item);
    // TODO: drupal_set_message
  }
}

/**
 * Menu callback for ct_item/add.
 *
 * Display the content tree add item form.
 */
function content_tree_item_add_page($parent_iid = 0) {
  global $user;
  $item = array(
    'iid' => 0,
    'parent_iid' => (int) $parent_iid,
    'item_title' => '',
    'item_path' => '',
    'name' => '',
    'uid' => $user->uid,
    'is_visible' => 1,
    'children' => 0,
  );
  drupal_set_title(t('Create content tree item'));
  return drupal_get_form('content_tree_item_form', $item);
}

/**
 * Menu callback for ct_item/%content_tree_item/edit.
 */
function content_tree_item_edit_page($item) {
  drupal_set_title(t('Edit content tree item'));
  return drupal_get_form('content_tree_item_form', $item);
}

/**
 * Return all locations defined for a content type separated by newline.
 *
 * The result of this function is used to fill in a "textarea" element
 * where the user can edit the locations.
 */
function _content_tree_type_get_locations_string($node_type) {
  $locations = array();
  foreach (content_tree_type_get_locations($node_type) as $parent_iid => $name_pattern) {
    $locations[] = (!$parent_iid ? '<root>' : (content_tree_get_path_alias($parent_iid)) . '|' . $name_pattern);
  }
  return implode("\n", $locations);
}

/**
 * Parse and save the locations defined for a content type.
 */
function _content_tree_type_save_locations_string($node_type, $locations, $default_name_pattern) {
  // Remove all assigned locations for the type.
  db_query("DELETE FROM {content_tree_node_location} WHERE node_type='%s'", $node_type);
  $saved = 0;
  foreach (explode("\n", $locations) as $loc) {
    list($path, $pattern) = array_map('trim', explode('|', $loc));
    if (!$path) {
      continue;
    }
    // Determine the parent item's ID from the path.
    if ($path == '<root>') {
      $parent_iid = 0;
    }
    elseif (!($parent_iid = content_tree_lookup_path($path))) {
      drupal_set_message(t("Couldn't find the content tree item located at <b>@path</b>. This location has been ignored.", array('@path' => $path)), 'error');
      continue;
    }
    db_query("INSERT INTO {content_tree_node_location} (node_type, parent_iid, name_pattern) VALUES('%s', %d, '%s')", $node_type, $parent_iid, ($pattern ? $pattern : $default_name_pattern));
    ++$saved;
  }
  return $saved;
}

/**
 * Add module-specific fields to the "node_type_form".
 */
function _content_tree_form_node_type_form_alter(&$form, $form_state) {
  $type_settings = variable_get("ct_type_config_{$form['#node_type']->type}", array('name_pattern' => CONTENT_TREE_NODE_NAME_PATTERN));

  $form['content_tree'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content tree'),
    '#tree' => TRUE,
  );
  $form['content_tree']['name_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Default name token pattern'),
    '#description' => t("The item's name is used to uniquely identify an item among its sibblings (children of the same parent) and to form the item's path alias."),
    '#default_value' => $type_settings['name_pattern'],
    '#required' => TRUE,
  );
  $form['content_tree']['locations'] = array(
    '#type' => 'textarea',
    '#title' => t('Locations'),
    '#description' => t('Specify one location per line as: <b>parent|name-token</b>. <i>parent</i> must be the path of an existing !treelink item. Use <b>&lt;root&gt;</b> as <i>parent</i> to insert at the tree root. If <i>name-token</i> is not specified then <b>Default name token pattern</b> will be used.', array('!treelink' => l(t('Content tree'), 'admin/content_tree'))),
    '#default_value' => _content_tree_type_get_locations_string($form['#node_type']->type),
    '#rows' => 3,
  );
  $form['#submit'][] = 'content_tree_form_node_type_form_submit';
}

/**
 * Utility function of submit handler for altered "node_type_form".
 */
function _content_tree_form_node_type_form_submit($form, &$form_state) {
  $type_settings = array('name_pattern' => $form_state['values']['content_tree']['name_pattern']);
  $type_settings['location_count'] = _content_tree_type_save_locations_string(
      $form_state['values']['type'], $form_state['values']['content_tree']['locations'], $form_state['values']['content_tree']['name_pattern']
  );
  if ($type_settings['location_count']) {
    drupal_set_message(format_plural($type_settings['location_count'], '1 location have been saved for this content type.', '@count locations have been saved for this content type.'));
  }
  else {
    drupal_set_message(t('You have not defined a default location for nodes of this content type.<br/>You can use the !treelink administration page to insert nodes of this type in the content tree.', array('!treelink' => l(t('Content tree'), 'admin/content_tree'))));
  }

  variable_set("ct_type_config_{$form_state['values']['type']}", $type_settings);
}
