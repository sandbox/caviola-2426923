<?php

/**
 * @file
 * Implement the utility functions to handle the content tree menu blocks.
 */

/**
 * Return the form to add a new content tree menu block.
 */
function content_tree_add_block_form(&$form_state) {
  module_load_include('inc', 'block', 'block.admin');
  return block_admin_configure($form_state, 'content_tree', NULL);
}

/**
 * Return the form to configure content tree menu blocks.
 */
function content_tree_configure_block_form($block = NULL) {
  if (empty($block['delta'])) {
    $form['_delta'] = array(
      '#type' => 'textfield',
      '#title' => t('Block identifier'),
      '#required' => TRUE,
      '#description' => t("This is the identification name of this block. This must be unique and will be used for the new block's <b>Delta</b>."),
      '#element_validate' => array('_content_tree_validate_block_delta'),
    );
  }
  $form['root_iid'] = array(
    '#type' => 'textfield',
    '#title' => t('Fixed parent item'),
    '#description' => t('The generated menu will only contain children of the specified item. <b>Starting level</b> is relative to the item specified here.'),
    '#default_value' => empty($block['root_iid']) ? '<root>' : content_tree_get_path_alias($block['root_iid']),
    '#element_validate' => array('_content_tree_validate_block_root_iid'),
  );
  $form['title_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Block title as link'),
    '#description' => t('Make the default block title a link to that menu item.'),
    '#default_value' => !empty($block['title_link']) ? $block['title_link'] : 0,
  );
  $form['start_level'] = array(
    '#type' => 'textfield',
    '#title' => t('Starting level'),
    '#size' => 5,
    '#description' => t('Menus starting at the 1st level will always be visible. Those starting at the 2nd level or deeper will only be visible when the trail to the active item in the menu tree.'),
    '#default_value' => !empty($block['start_level']) ? $block['start_level'] : 1,
  );
  $form['follow_trail'] = array(
    '#type' => 'select',
    '#title' => t('Follow active trail'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes, show active item'),
      2 => t('Yes, show children of active item'),
    ),
    '#description' => '',
    '#default_value' => !empty($block['follow_trail']) ? $block['follow_trail'] : 0,
  );

  $form['max_depth'] = array(
    '#type' => 'value',
    '#value' => 10,
  );
  return $form;
}

/**
 * Submit handler for "content_tree_add_block_form".
 */
function content_tree_add_block_form_submit($form, &$form_state) {
  $values = &$form_state['values'];

  $inserted = db_query("INSERT INTO {content_tree_menu_block} (delta, root_iid, start_level, max_depth, follow_trail, title_link) VALUES('%s', %d, %d, %d, %d, %d)", $values['_delta'], $values['root_iid'], $values['start_level'], $values['max_depth'], $values['follow_trail'], $values['title_link']);
  if (!$inserted) {
    drupal_set_message(t('The Content tree block could not be created.'), 'error');
    return;
  }

  // CODE BORROWED FROM block_add_block_form_submit() in block.admin.inc.
  foreach (list_themes() as $theme) {
    if ($theme->status) {
      db_query("INSERT INTO {blocks} (visibility, pages, custom, title, module, theme, status, weight, delta, cache) VALUES(%d, '%s', %d, '%s', '%s', '%s', %d, %d, '%s', %d)", $values['visibility'], trim($values['pages']), $values['custom'], $values['title'], $values['module'], $theme->name, 0, 0, $values['_delta'], BLOCK_NO_CACHE);
    }
  }

  foreach (array_filter($values['roles']) as $rid) {
    db_query("INSERT INTO {blocks_roles} (rid, module, delta) VALUES (%d, '%s', '%s')", $rid, $values['module'], $values['_delta']);
  }

  drupal_set_message(t('The Content tree block has been created.'));
  cache_clear_all();

  $form_state['redirect'] = 'admin/build/block';
}

/**
 * Validator function for field "delta".
 */
function _content_tree_validate_block_delta($element, &$form_state) {
  if (!preg_match('/^[a-z0-9_\-]+$/', $form_state['values']['_delta'])) {
    form_set_error('_delta', t('The block identifier name must contain only lowercase letters, numbers, underscores and hypens.'));
  }
  if (db_result(db_query("SELECT 1 FROM {content_tree_menu_block} WHERE delta='%s' LIMIT 1", $form_state['values']['_delta']))) {
    form_set_error('_delta', t('The block identifier %delta is already taken.', array('%delta' => $form_state['values']['_delta'])));
  }
}

/**
 * Validator function for field "root_iid".
 */
function _content_tree_validate_block_root_iid($element, &$form_state) {
  if ($form_state['values']['root_iid'] == '<root>') {
    form_set_value($element, 0/* root IID */, $form_state);
  }
  elseif (($root_iid = content_tree_lookup_path($form_state['values']['root_iid']))) {
    form_set_value($element, $root_iid, $form_state);
  }
  else {
    form_set_error('root_iid', t('You must provide the path of an existing Content tree item.'));
  }
}
