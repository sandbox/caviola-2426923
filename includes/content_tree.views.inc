<?php

/**
 * @file
 * Implement View integration hooks and utility functions.
 */

/**
 * Implements hook_views_handlers().
 */
function content_tree_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'content_tree') . '/includes',
    ),
    'handlers' => array(
      'content_tree_handler_field_path' => array('parent' => 'views_handler_field'),
      'content_tree_handler_field_title' => array('parent' => 'views_handler_field'),
    ),
  );
}

/**
 * Implements hook_views_data_alter().
 */
function content_tree_views_data_alter(&$data) {
  // Here we tell Views 2 that the base table "node" is joinable with
  // the "content_tree_item" base table through their common "nid" field.
  // Thus, in a view having "content_tree_item" as the primary table,
  // the fields from ALL tables that have "node" as base
  // (notoriously CCK tables) are automatically available.
  $data['node']['table']['join']['content_tree_item'] = array(
    'left_field' => 'nid',
    'field' => 'nid',
  );
}

/**
 * Implements hook_views_data().
 */
function content_tree_views_data() {
  $data['content_tree_item']['table']['group'] = t('Content tree');
  $data['content_tree_item']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['content_tree_item']['table']['base'] = array(
    'title' => t('Content tree item'),
    'field' => 'iid',
    'help' => t('This table stores all items in the Content tree.'),
  );
  $data['content_tree_item']['iid'] = array(
    'title' => t('IID'),
    'help' => t('The item ID'),
    'field' => array('handler' => 'views_handler_field_numeric'),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
  );
  $data['content_tree_item']['parent_iid'] = array(
    'title' => t('Parent ID'),
    'help' => t("The item's parent ID."),
    'field' => array('handler' => 'views_handler_field_numeric'),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
    'argument' => array('handler' => 'views_handler_argument'),
    'relationship' => array(
      'base' => 'content_tree_item',
      'field' => 'iid',
      'handler' => 'views_handler_relationship',
      'label' => t('Parent item'),
    ),
  );
  $data['content_tree_item']['nid'] = array(
    'title' => t('Node ID'),
    'help' => t('The associated node ID, if any.'),
    'field' => array('handler' => 'views_handler_field_numeric'),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
    'argument' => array('handler' => 'views_handler_argument'),
  );
  $data['content_tree_item']['name'] = array(
    'title' => t('Name'),
    'help' => t('The item name.'),
    'field' => array('handler' => 'views_handler_field_string'),
  );
  $data['content_tree_item']['title'] = array(
    'title' => t('Title'),
    'help' => t('The item title.'),
    'field' => array(
      'handler' => 'content_tree_handler_field_title',
      'additional fields' => array(
        'node_title' => array('field' => 'title', 'table' => 'node'),
        'item_title' => 'item_title',
      ),
    ),
  );
  $data['content_tree_item']['path'] = array(
    'title' => t('Path'),
    'help' => t('The internal or external path.'),
    'field' => array(
      'handler' => 'content_tree_handler_field_path',
      'additional fields' => array(
        'iid' => 'iid',
        'nid' => 'nid',
        'is_external' => 'is_external',
        'item_path' => 'item_path',
      ),
    ),
  );
  $data['content_tree_item']['weight'] = array(
    'title' => t('Weight'),
    'help' => t('The item weight.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array('handler' => 'views_handler_sort'),
  );
  $data['content_tree_item']['is_visible'] = array(
    'title' => t('Is visible'),
    'help' => t('Whether the item is currently visible.'),
    'field' => array('handler' => 'views_handler_field_boolean'),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Visible'),
      'type' => 'yes-no',
      // Use boolean_field = 1 instead of boolean_field <> 0 in WHERE statament.
      'use equal' => TRUE,
    ),
  );
  $data['content_tree_item']['is_external'] = array(
    'title' => t('Is external'),
    'help' => t('Whether the path of a "link item" is external.'),
    'field' => array('handler' => 'views_handler_field_boolean'),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('External'),
      'type' => 'yes-no',
      // Use boolean_field = 1 instead of boolean_field <> 0 in WHERE statament.
      'use equal' => TRUE,
    ),
  );
  $data['content_tree_item']['children'] = array(
    'title' => t('Children'),
    'help' => t("The item's children count. Leaf items has -1 in this field."),
    'field' => array('handler' => 'views_handler_field_numeric'),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
  );
  return $data;
}
