<?php

/**
 * @file
 * Implement a field handler class for Views integration.
 */

/**
 * Make the "path" content tree item field available in Views.
 */
class content_tree_handler_field_path extends views_handler_field {

  public function allow_advanced_render() {
    return FALSE;
  }

  public function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  public function option_definition() {
    return array();
  }

  public function render($values) {
    $iid = $values->{$this->aliases['iid']};
    $nid = $values->{$this->aliases['nid']};
    $is_external = $values->{$this->aliases['is_external']};
    $item_path = $values->{$this->aliases['item_path']};

    if ($nid) {
      return "ct_node/$iid";
    }
    if (!$is_external) {
      return "ct_item/$iid";
    }
    else {
      return $item_path;
    }
  }

}
