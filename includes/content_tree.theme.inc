<?php

/**
 * @file
 * Implement theme hooks and utility functions.
 */

/**
 * Implements theme_content_tree_admin_form().
 *
 * Theme the content tree administration form.
 */
function theme_content_tree_admin_form($form) {
  drupal_add_css(drupal_get_path('module', 'content_tree') . '/css/content_tree.css');

  $header = array(
    '',
    '',
    t('Title'),
    t('Type'),
    t('Visibility'),
    t('Operations'),
  );
  $rows = array();

  if (!empty($form['items'])) {
    drupal_add_tabledrag('ContentTree_Table', 'order', 'sibling', 'weight');

    foreach (element_children($form['items']) as $iid) {
      $row = array(
        array('data' => drupal_render($form['items'][$iid]), 'class' => 'select-item'),
        drupal_render($form['weight'][$iid]),
        drupal_render($form['title'][$iid]),
        drupal_render($form['type'][$iid]),
        drupal_render($form['is_visible'][$iid]),
        drupal_render($form['edit'][$iid]),
      );
      $rows[] = array('data' => $row, 'class' => 'draggable');
    }
  }
  else {
    $rows[] = array(
      array(
        'data' => t('You have not created content at this location yet.'),
        'colspan' => 6,
        'align' => 'center'),
    );
  }

  $output = drupal_render($form['breadcrumb']);
  if (!empty($form['parent_link'])) {
    $output .=
        '<div id="ContentTree_Path">' .
        t('Location URL:') .
        ' <span class="path">' .
        drupal_render($form['parent_link']) .
        '</span></div>';
  }

  $output .= drupal_render($form['menu']);
  $output .= theme('table', $header, $rows, array('id' => 'ContentTree_Table'));

  if (!empty($form['items'])) {
    $output .= drupal_render($form['update']);
    $output .= drupal_render($form['move']);
  }

  $output .= theme('pager');
  $output .= drupal_render($form);

  return $output;
}

/**
 * Implements theme_content_tree_page_menu().
 *
 * This function renders a menu block, which includes a title and a subtree.
 */
function theme_content_tree_page_menu($data) {
  if (empty($data['parents']) || empty($data['parent_items'])) {
    return '';
  }
  $data['parents'] = array_reverse($data['parents'], TRUE);
  return _content_tree_page_menu_output($data['parents'], $data['parent_items']);
}

/**
 * Render a subtree as a menu tree.
 */
function _content_tree_page_menu_output(&$parents, $parent_items) {
  $output = '';
  $parent_iid = array_pop($parents);

  foreach ($parent_items[$parent_iid] as $item) {
    $options = array('attributes' => array('class' => $item['#class']));

    $link = l($item['title'], $item['is_external'] ? $item['item_path'] : $item['path'], $options);

    $output .= !empty($parent_items[$item['iid']]) ?
        theme('menu_item', $link, $item['children'] > 0, _content_tree_page_menu_output($parents, $parent_items), TRUE/* in active trail */) :
        theme('menu_item', $link, $item['children'] > 0);
  }

  return theme('menu_tree', $output);
}
