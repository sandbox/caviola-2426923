<?php

/**
 * @file
 * Implement a field handler class for View integration.
 */

/**
 * Make the "title" content tree item field available in Views.
 */
class content_tree_handler_field_title extends views_handler_field {

  public function allow_advanced_render() {
    return FALSE;
  }

  public function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  public function render($values) {
    $node_title = $values->{$this->aliases['node_title']};
    $item_title = $values->{$this->aliases['item_title']};

    return $node_title ? $node_title : $item_title;
  }

}
