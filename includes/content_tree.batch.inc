<?php

/**
 * @file
 * Implement Batch API hooks and utility functions.
 */

/**
 * Delete a list of content tree items.
 */
function _content_tree_batch_delete($items, &$context) {
  if (!isset($context['sandbox']['items'])) {
    $context['sandbox']['items'] = $items;
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['total'] = count($items);
  }

  for ($processed = 0; $processed <= CONTENT_TREE_BATCH_DELETE_THRESHOLD; ++$processed) {
    if (!($iid = array_shift($context['sandbox']['items']))) {
      // Done.
      return;
    }
    if (content_tree_item_delete($iid)) {
      ++$context['results']['deleted'];
    }
  }
  $context['sandbox']['progress'] += $processed;

  if ($context['sandbox']['progress'] < $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['total'];
  }
}

/**
 * Called when content_tree_delete_batch_process() has finished.
 */
function _content_tree_batch_delete_finished($success, $results, $operations) {
  if ($success && !empty($results)) {
    drupal_set_message(t('!count content tree item(s) deleted successfully.', array('!count' => $results['deleted'])));
  }
}

/**
 * Update the path alias of a list of content tree items.
 */
function _content_tree_batch_update_path(&$context) {
  if (!isset($context['sandbox']['update_root_iid'])) {
    $context['sandbox']['update_root_iid'] = db_result(db_query(
            'SELECT iid FROM {content_tree_item} WHERE flag = %d', CONTENT_TREE_FLAG_UPDATE_PATH_ROOT));

    if (!$context['sandbox']['update_root_iid']) {
      return;
    }

    $context['sandbox']['total'] = db_result(db_query(
            'SELECT count(*) FROM {content_tree_closure} WHERE ancestor = %d', $context['sandbox']['update_root_iid']));

    $context['sandbox']['start'] = 0;
  }

  $limit = 100;

  // Get $limit children of item $update_root_iid (inclusive)
  // starting at $start. The results are ordered so that
  // an item comes after its parent.
  $result = db_query(
      "SELECT
      i.iid, i.parent_iid, i.nid, i.name
    FROM {content_tree_item} i
      JOIN {content_tree_closure} c ON iid = descendant
    WHERE c.ancestor = %d
    ORDER BY c.depth, i.parent_iid
    LIMIT %d, $limit", $context['sandbox']['update_root_iid'], $context['sandbox']['start']);

  $updated = 0;
  $parent_iid = -1;
  while (($item = db_fetch_array($result))) {
    if ($item['parent_iid'] != $parent_iid) {
      $parent_iid = $item['parent_iid'];
      $parent_path = content_tree_get_path_alias($parent_iid);
    }
    content_tree_set_path_alias($item, $parent_path);
    ++$updated;
  }
  if (!$updated) {
    return;
  }

  $context['results']['updated'] += $updated;
  $context['sandbox']['start'] += $updated;
  if ($context['sandbox']['start'] < $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['start'] / $context['sandbox']['total'];
  }
  else {
    // We are done.
    // Remove the "update flag" from the item.
    db_query('UPDATE {content_tree_item} SET flag = 0 WHERE iid = %d', $context['sandbox']['update_root_iid']);
  }
}

/**
 * Called when content_tree_update_path_batch_process() has finished.
 */
function _content_tree_batch_update_path_finished($success, $results, $operations) {
  if ($success && !empty($results)) {
    drupal_set_message(t('!count path alias(es) updated successfully.', array('!count' => $results['updated'])));
  }
}
